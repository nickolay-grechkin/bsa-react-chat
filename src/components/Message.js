import React from 'react';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import FavoriteIcon from '@material-ui/icons/Favorite';
import IconButton from '@material-ui/core/IconButton';
import dateFormat from 'dateformat';

class Message extends React.Component {
    
    render () {
        return (
            <div className="message">
                <CardContent className="message">                                     
                    <Avatar className="message-user-avatar" src={this.props.message.avatar}/>
                    <Typography className="message-user-name" color="textSecondary" gutterBottom>                                       
                        {this.props.message.user}
                    </Typography> 
                    <Typography variant="h5" component="h2" className="message-text">
                        {this.props.message.text}
                    </Typography>
                    <Typography align="right" variant="body2" className="message-time">
                        {dateFormat(this.props.message.createdAt, 'UTC: HH:MM')}
                    </Typography>
                    <IconButton className="message-liked" onClick={() => {this.props.handleLikeMessage(this.props.message.id)}}>
                        <FavoriteIcon className={this.props.message.isLiked ? "message-liked":"message-like"} />
                    </IconButton>  
                </CardContent>
            </div>
        );
    }
}

export default Message