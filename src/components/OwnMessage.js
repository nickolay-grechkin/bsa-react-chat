import React from 'react';
import Typography from '@material-ui/core/Typography';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import HighlightOffIcon from '@material-ui/icons/HighlightOff';
import IconButton from '@material-ui/core/IconButton';
import EditIcon from '@material-ui/icons/Edit';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import dateFormat from 'dateformat';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';

function OwnMessage({message, handleDeleteMessage, handleUpdateMessage}) {
    const [open, setOpen] = React.useState(false);
    const [updateMessage, setMessage] = React.useState(null);
    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const onChange = (e) => {
        setMessage(e.target.value);
    }

    return (
        <div className="own-message">
            <CardContent >                                   
                <Avatar className="own-message-avatar" color="textSecondary" src={message.avatar}/>
                <IconButton color="secondary" aria-label="delete" className="message-delete" onClick={() => {handleDeleteMessage(message.id)}}>                                       
                    <HighlightOffIcon />
                </IconButton> 
                <IconButton color="secondary" aria-label="delete" className="message-edit" onClick={handleClickOpen}>                                       
                    <EditIcon />
                </IconButton> 
                <Typography variant="h5" component="h2" className="message-text">
                    {message.text}
                </Typography>
                <Typography align="right" variant="body2" className="message-time">
                    {dateFormat(message.createdAt, 'UTC: HH:MM')}
                </Typography>  
            </CardContent>
            <Dialog open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Update message</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                    </DialogContentText>
                    <TextareaAutosize
                        autoFocus
                        margin="dense"
                        id="name"
                        label="Email Address"
                        onChange = {(e) => onChange(e)}
                        type="email"
                    />
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Close
                    </Button>
                    <Button onClick={() => handleUpdateMessage(message.id, updateMessage)} color="primary">
                        Update
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    )
}

export default OwnMessage