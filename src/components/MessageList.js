import React from 'react';
import Container from '@material-ui/core/Container';
import Message from './Message';
import OwnMessage from './OwnMessage';

class MessageList extends React.Component {
    render() {
      return (
        <div className="message-list">
          {this.props.messages.map(message => {
            if (message.userId === this.props.currentUser.userId) {
                return (
                  <Container key={message.id}>
                    <OwnMessage 
                    message={message} 
                    handleDeleteMessage={this.props.handleDeleteMessageById} 
                    handleUpdateMessage={this.props.handleUpdateMessage} />
                  </Container>
                  );
              }
              else {
                return (
                  <Container key={message.id}>
                    <Message 
                      message={message} 
                      handleLikeMessage={this.props.handleLikeMessage} />
                  </Container>
                );
              }
          })}
        </div>
              
      );
    }
}
export default MessageList