import React from 'react';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import DirectionsIcon from '@material-ui/icons/Directions';
import InputBase from '@material-ui/core/InputBase';


class MessageInput extends React.Component {
    newMessage = () => {
        this.props.handleAddNewMessage(this.state.message);
    }

    onChange = (e) => {
        this.setState ({
            message: {
            "text": e.target.value,
    }
        });
    };
    render() {
        return (
            <Paper className="message-input" position="fixed" component="form">
                <InputBase className="message-input-text"
                placeholder="Search Google Maps"
                inputProps={{ 'aria-label': 'search google maps' }}
                onChange = {this.onChange}
                />
                <IconButton className="message-input-button" color="primary" aria-label="directions" onClick={ this.newMessage }>
                <DirectionsIcon />
                </IconButton>
            </Paper>
        );
    }
}

export default MessageInput