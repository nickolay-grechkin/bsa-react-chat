import React from 'react';
import './Chat.css';
import logo from '../logo.svg'

function Preloader() {
    return (
      <div className="preloader">
        <header className="preloader-header">
          <img src={logo} className="preloader-logo" alt="logo" />
          <p>
            Load data
          </p>
        </header>
      </div>
    );
  }

export default Preloader