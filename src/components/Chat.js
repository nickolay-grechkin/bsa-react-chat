// import logo from './logo.svg';
import './Chat.css';
import React from 'react';
import Header from './Header'
import MessageList from './MessageList'
import MessageInput from './MessageInput'
import Preloader from './Preloader';
import {v4 as uuidv4} from 'uuid';

class Chat extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      messages: [],
      isLiked: false,
      currentUser: {
        "userId": "b31b3570-8615-446f-96bd-8f70b516431d",
        "avatar": "https://st3.depositphotos.com/15648834/17930/v/600/depositphotos_179308454-stock-illustration-unknown-person-silhouette-with-glasses.jpg",
        "user": "Nick",
        "text": "",
        "createdAt": "",
        "editedAt": ""
      }
    };
  }

  componentDidMount() {
    console.log("render");
    fetch(this.props.url)
    .then(res => res.json())
    .then(
      (result) => {
        result.sort(function(a,b){
          return new Date(a.createdAt) - new Date(b.createdAt);
        });
        this.setState({
          isLoaded: true,
          messages: result
        });
      },
      (error) => {
        this.setState({
          isLoaded:true,
          error
        });
      }
    )
  }

  reloadData = () => {
    this.setState({
      isLoaded: this.state.isLoaded
    });
  }

  handleAddMessage(message) {
    const newMessage = {"id": uuidv4(),
            "userId": this.state.currentUser.userId,
            "avatar": this.state.currentUser.avatar,
            "user": this.state.currentUser.user,
            "text": message.text,
            "createdAt": new Date().toISOString(),
            "editedAt": new Date()
  }
    this.state.messages.push(newMessage);
    this.reloadData();
  }

  handleDeleteMessage(id) {
    const message = this.state.messages.find(msg => msg.id === id);
    const index = this.state.messages.indexOf(message);
    this.state.messages.splice(index, 1);
    this.reloadData();
  }

  handleLikeMessage(id) {
    const message = this.state.messages.find(msg => msg.id === id);
    const index = this.state.messages.indexOf(message);
    this.state.messages[index].isLiked = !this.state.messages[index].isLiked;
    this.reloadData();
  }

  handleUpdateMessage(id, editedText) {
    if (editedText != null) {
      const message = this.state.messages.find(msg => msg.id === id);
      const index = this.state.messages.indexOf(message);
      this.state.messages[index].text = editedText;
      this.reloadData();
    }
  }

  render() {
    const { error, isLoaded, messages } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>
    } else if (!isLoaded) {
      return <div><Preloader /></div>;
    } else {
      return (
        <div className="chat">
          <Header 
          messages={messages}
          
          />
          <MessageList 
          messages={messages} 
          handleDeleteMessageById={this.handleDeleteMessage.bind(this)} 
          handleUpdateMessage={this.handleUpdateMessage.bind(this)} 
          handleLikeMessage = {this.handleLikeMessage.bind(this)} 
          currentUser = {this.state.currentUser} 
            
          />
          <MessageInput 
          handleAddNewMessage={this.handleAddMessage.bind(this)} 
          currentUser={this.state.currentUser} />
        </div> 
      );
    }
  }
}

export default Chat;
